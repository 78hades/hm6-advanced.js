
class Address {
    constructor(address, dataContinent) {
        this.country = address.country;
        this.region = address.region;
        this.continent = dataContinent.continent;
        this.city = address.city;
    }

    newMethod() {
        const container = document.querySelector('.container');
        const continent = document.querySelector('.continent').innerHTML = `Continent: ${this.continent}`
        const country = document.querySelector('.country').innerHTML = `Country: ${this.country}`
        const region = document.querySelector('.region').innerHTML = `Region: ${this.region}`
        const city = document.querySelector('.city').innerHTML = `City: ${this.city}`
    }
}

const button = document.querySelector('button');

async function getIp() {
    try {
        const response = await fetch('http://api.ipify.org/?format=json');
        const userData = await response.json();
        return userData.ip;
    } catch (error) {
        throw new Error(error);
    }
}

async function getPhysicalAddress() {
    try {
        const ip = await getIp();
        const response = await fetch(`http://ip-api.com/json/${ip}`);
        const dataResponceContinent = await fetch(`http://ip-api.com/json/${ip}?fields=continent`);
        const dataContinent = await dataResponceContinent.json()
        const userData = await response.json();
        const object =  new Address(userData, dataContinent);
        object.newMethod();
    } catch (error) {
        throw new Error(error);
    }
}

button.addEventListener('click', async () => {
    try {
        const address = await getPhysicalAddress();
    } catch (error) {
       throw new Error(error)
    }
});